# Index of Articles

| Sr. no | Article Title | Author | Moderator | Submitted on | Moderated on | Published | Published Link |
|--------|---------------|--------|-----------|--------------|--------------|-----------|----------------|
|1|[Challenges & Risks in Cloud Computing](/articles/Challenges%20in%20Cloud%20Computing,%20Vipul%20Kushwaha.md)|Prof. Vipul Kushwaha|Saomyavandit|15/11/2023|-|No|-|
|2|[A Journey to Self Worth](/articles/A%20Journey%20to%20Self%20Worth,Miheera%20Nikam.md)|Miheera Nikam|Prof. Vipul Kushwaha|15/11/2023|14/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/a-journey-to-self-worth)|
|3|[Importance of Self-care](/articles/importance%20of%20self%20care,jiya%20gudhaka.md)|Jiya Gudhaka|Miheera Nikam|17/11/2023|14/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/importance-of-self-care)|
|4|[Time for Yourself](/articles/TIME_FOR_YOURSELF_Janhavi_Tayade.md)|Janhavi Tayade|Jiya Gudhaka|17/11/2023|15/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/time-for-yourself)|
|5|[The Grand Line Awaits! Setting Sail with One Piece](/articles/The%20Grand%20Line%20Awaits!%20Setting%20Sail%20with%20One%20Piece,%20Greenal%20Tambe.md)|Greenal Tambe|Varun Jhaveri|17/11/2023|15/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/the-grand-line-awaits-setting-sail-with-one-piece)|
|6|[Exploring the Boundless World of 3D Printing](/articles/Exploring%20the%20Boundless%20World%20of%203D%20Printing,Shreyash.md)|Shreyansh|Greenal Tambe|17/11/2023|16/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/exploring-the-boundless-world-of-3d-printing)|
|7|[The Impact of Artificial Intelligence on Everyday Life](/articles/The%20Impact%20of%20Artificial%20Intelligence%20on%20Everyday%20Life,%20Aditya%20Choudhary.md)|Aditya Choudhary|Shreyansh|17/11/2023|16/01/2024|Yes|[Link](https://buzzworthy.co.in/blog/the-impact-of-artificial-intelligence-on-everyday-life)|
|8|[Zooming into world of cars](/articles/Zooming%20into%20world%20of%20cars%20,%20Nitin%20Sharma.md)|Nitin Sharma|Saomyavandit|17/11/2023|21/11/2023|Yes|[Link](https://buzzworthy.co.in/blog/zooming-into-the-world-of-cars-a-magical-adventure)|
|9|[AR-VR Technology](/articles/AR-VR.md)|Arsalaan Khan|Saomyavandit|17/11/2023|21/11/2023|Yes|[Link](https://buzzworthy.co.in/blog/ar-vr-technology)|
|10|[Glowing Realm of Organisms](/articles/glowing%20realm%20of%20organisms,%20Varun%20Jhaveri.md)|Varun Jhaveri|Saomyavandit|17/11/2023|20/11/2023|Yes|[Link](https://buzzworthy.co.in/blog/a-journey-into-the-glowing-realm-of-living-organisms)|
|11|[Van Gogh](/articles/Van%20Gogh,%20Shreya%20Mahajan.md)|Shreya Mahajan|Saomyavandit|18/11/2023|20/11/2023|Yes|[Link](https://buzzworthy.co.in/blog/van-gogh-the-dutch-post-impressionist-painter)|
|12|[Life Lessons from One Piece](/articles/Life%20Lessons%20from%20One%20Piece,%20Mohit%20Agarwal.md)|Mohit Agarwal|Saomyavandit|20/11/2023|22/11/2023|Yes|[Link](https://buzzworthy.co.in/blog/life-lessons-and-values-we-can-learn-from-watching-one-piece-anime)|
|13|[How AI Works](/articles/how_ai_works.md)|Admin|Saomyavandit|24/12/2023|26/12/2024|Yes|[Link](https://buzzworthy.co.in/blog/understanding-ai-an-introduction-to-artificial-intelligence-and-how-it-works)|
|14|[The Power of Expert Guidance](articles/SoloSynergy,%20Vidhita%20Jagwani.md)|Vidhita Jagwani|Saomyavandit|02/02/2024|04/02/2024|Yes|[Link](https://buzzworthy.co.in/blog/solosynergy-the-power-of-expert-guidance-for-thriving-developers)|
|15|[Cyberpunk](/articles/Cyberpunk,%Siddh%Jain.md)|Siddh Jain|Saomyavandit|24/12/2023|14/02/2024|Yes|[Link](https://buzzworthy.co.in/blog/the-dazzling-world-of-cyberpunk)|
|16|[Ikigai](articles/Ikigai,%20Prinka%20Devi.md)|Prinka Devi|Saomyavandit|11/01/2024|-|no|-|
|17|[NieR: Automata: A Symphony of Machines and Souls](articles/NieR:Automata%20,%20Arnav_Joglekar.md)|Saomyavandit|-|15/02/2024|-|no|-|
